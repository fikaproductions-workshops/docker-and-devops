<?php

$configs = [];

$configs['languages'] = [
    'fr' => 'French',
    'en' => 'English',
];

if (!empty(getenv('COCKPIT_DB_CONNECTION_STRING'))) {
    $configs['database'] = [
        'server' => getenv('COCKPIT_DB_CONNECTION_STRING') . '/' . getenv('COCKPIT_DB'),
        'options' => ['db' => getenv('COCKPIT_DB')],
    ];
}

return $configs;

